package com.uploadcsv.resource;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

import com.uploadcsv.domain.Cidade;

public class CidadeResource extends Resource<Cidade> {
	
	public CidadeResource(Cidade cidade, Link... links) {
		super(cidade, links);
	}
	
}