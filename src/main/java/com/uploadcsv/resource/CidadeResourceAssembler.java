package com.uploadcsv.resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.uploadcsv.controller.CidadeController;
import com.uploadcsv.domain.Cidade;

public class CidadeResourceAssembler extends ResourceAssemblerSupport<Cidade, CidadeResource> {
	
	public CidadeResourceAssembler() {
		super(Cidade.class, CidadeResource.class);
	}

	@Override
	public CidadeResource toResource(Cidade cidade) {
		return new CidadeResource(cidade, linkTo(methodOn(CidadeController.class).get(cidade.getId())).withSelfRel());
	}
	
	@Override
	protected CidadeResource instantiateResource(Cidade cidade) {
		return new CidadeResource(cidade);
	}

}