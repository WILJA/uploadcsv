package com.uploadcsv.dto;

public class UfDto {
	private Long count;
	private String uf;

	public UfDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UfDto(Long count, String uf) {
		super();
		this.count = count;
		this.uf = uf;
	}

	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}

	
	
	
}
