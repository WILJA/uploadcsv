package com.uploadcsv.dto;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class FileDto {
	private String name;
	private String originalName;
	private String extension;
	private String contentType;
	private InputStream inputStream;
	private byte[] bytes;

	public FileDto() {}

	public FileDto(String name, String contentType, InputStream inputStream) {
		this.name = name;
		this.contentType = contentType;
		this.inputStream = inputStream;
	}
	
	public FileDto(String name, String contentType, byte[] bytes) {
		this.name = name;
		this.contentType = contentType;
		this.bytes = bytes;
	}

	public FileDto(MultipartFile multipartFile) {
		this.name = multipartFile.getOriginalFilename();
		this.originalName = multipartFile.getOriginalFilename();
		this.contentType = multipartFile.getContentType();
		this.extension = this.name.substring(this.name.lastIndexOf(".") + 1);
		try {
			this.inputStream = multipartFile.getInputStream();
		} catch (IOException e) {
		}
	}

	public String getSimpleName() {
		if (this.name == null) {
			return null;
		}
		return this.name.substring(this.name.lastIndexOf("/") + 1);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
}
