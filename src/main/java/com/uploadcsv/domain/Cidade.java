package com.uploadcsv.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Setter
public class Cidade {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private Long ibgeId;
	private String uf;
	private String name;
	private String capital;
	private String lon;
	private String lat;
	private String noAccents;
	private String alternativeNames;
	private String microRegion;
	private String mesoRegion;
	
}
