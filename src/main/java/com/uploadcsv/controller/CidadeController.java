package com.uploadcsv.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.upload.util.UploadCvsUtil;
import com.uploadcsv.domain.Cidade;
import com.uploadcsv.dto.UrlDto;
import com.uploadcsv.repository.CidadeRepository;
import com.uploadcsv.resource.CidadeResource;
import com.uploadcsv.resource.CidadeResourceAssembler;
import com.uploadcsv.service.CidadeService;

@RestController
@RequestMapping("/cidades")
public class CidadeController {

	@Autowired
	CidadeRepository repository;

	@Autowired
	CidadeService service;

	CidadeResourceAssembler assembler = new CidadeResourceAssembler();


	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Page<Cidade>> getAll(@PageableDefault(page = 0, size = 10) Pageable pageable) {
		return new ResponseEntity<Page<Cidade>>(repository.findAll(pageable), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Cidade> get(@PathVariable Long id) {
		Cidade cidade = repository.findOne(id);
		if (cidade != null) {
			return new ResponseEntity<Cidade>(cidade, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping
	public ResponseEntity<CidadeResource> create(@RequestBody Cidade cidade) {
		cidade = repository.save(cidade);
		if (cidade != null) {
			return new ResponseEntity<>(assembler.toResource(cidade), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<CidadeResource> update(@PathVariable Long id, @RequestBody Cidade cidade) {
		if (cidade != null) {
			cidade.setId(id);
			cidade = repository.save(cidade);
			return new ResponseEntity<>(assembler.toResource(cidade), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}


	@DeleteMapping("/{id}")
	public ResponseEntity<CidadeResource> delete(@PathVariable Long id) {
		Cidade cidade = repository.findOne(id);
		if (cidade != null) {
			repository.delete(cidade);
			return new ResponseEntity<>(assembler.toResource(cidade), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	@GetMapping("/value/{value}")
	public ResponseEntity<Page<Cidade>> findByValue(@PathVariable String value, @PageableDefault(page = 0, size = 10) Pageable pageable) {
		return new ResponseEntity<Page<Cidade>>(repository.findByNameContaining(value, pageable), HttpStatus.OK);
	}
	

	@RequestMapping(value = "/uploadcsv", method = RequestMethod.POST)
	public ResponseEntity<List<Cidade>> uploadCidadeCsv(@RequestBody UrlDto file) {
		List<Cidade> cidades = service.uploadCidadeCsv(UploadCvsUtil.multipart(file.getUrl()));

		if (cidades != null && !cidades.isEmpty()) {
			return new ResponseEntity<List<Cidade>>(cidades, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/ufcount")
	public ResponseEntity<List<Cidade>> countUf() {
		return new ResponseEntity<List<Cidade>>(repository.findByCountUf(), HttpStatus.OK);
	}
}
