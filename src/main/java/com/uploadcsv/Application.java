package com.uploadcsv;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.uploadcsv.domain.User;
import com.uploadcsv.repository.CidadeRepository;
import com.uploadcsv.repository.UserRepository;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;

@SpringBootApplication
@Import(SpringDataRestConfiguration.class)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setMaxUploadSize(-1);
		return multipartResolver;
	}

	/**
	 * Save users and students to H2 DB for testing
	 * 
	 * @param repository
	 * @return
	 */
	@Bean
	public CommandLineRunner demo(UserRepository userRepository) {
		return (args) -> {

			// Create users with BCrypt encoded password (user/123, admin/123)
			User user1 = new User("user", "$2a$10$mEzsmunFqixlfBImm7LdJuhtfX.nVsjBZpZlpuC2QW3NGCJi1EMKm", "USER");
			User user2 = new User("admin", "$2a$10$mEzsmunFqixlfBImm7LdJuhtfX.nVsjBZpZlpuC2QW3NGCJi1EMKm", "ADMIN");
			userRepository.save(user1);
			userRepository.save(user2);
		};
	}
}
