package com.uploadcsv.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.uploadcsv.domain.Cidade;
import com.uploadcsv.repository.CidadeRepository;

@Service
public class CidadeService {

	private static final Logger logger = LoggerFactory.getLogger(CidadeService.class);

	@Autowired
	CidadeRepository cidadeRepository;

	public List<Cidade> uploadCidadeCsv(MultipartFile file) {
		List<Cidade> cidades = new ArrayList<Cidade>();
		List<Long> cidadesIbgeIds = null;
		try {
			List<Cidade> cidadesAux = cidadeRepository.findAll();

			if (cidadesAux != null && !cidadesAux.isEmpty()) {
				cidadesIbgeIds = cidadesAux.stream().map(obj -> obj.getIbgeId()).collect(Collectors.toList());
			}
			final String label = UUID.randomUUID().toString() + ".csv";
			final String filepath = "/tmp/" + label;
			byte[] bytes = file.getBytes();
			File fh = new File("/tmp/");
			if (!fh.exists()) {
				fh.mkdir();
			}

			FileOutputStream writer = new FileOutputStream(filepath);
			writer.write(bytes);
			writer.close();

			logger.info("image bytes received: {}", bytes.length);

			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filepath)));
			String line;
			boolean firstLine = true;
			while ((line = reader.readLine()) != null) {
				if (firstLine) {
					firstLine = false;
					continue;
				}
				Cidade cidade = new Cidade();

				String[] parts = line.split(",");

				String ibgeId = parts[0];

				if (cidadesIbgeIds != null && !cidadesIbgeIds.isEmpty() && ibgeId != null && !ibgeId.isEmpty()
						&& cidadesIbgeIds.contains(Long.parseLong(ibgeId))) {
					continue;
				}

				String uf = parts[1];
				String name = parts[2];
				String capital = parts[3];
				String lon = parts[4];
				String lat = parts[5];
				String noAccents = parts[6];
				String alternativeNames = parts[7];
				String microregion = parts[8];
				String mesoregion = parts[9];

				cidade.setIbgeId(Long.parseLong(ibgeId));
				cidade.setUf(uf);
				cidade.setName(name);
				cidade.setCapital(capital);
				cidade.setLon(lon);
				cidade.setLat(lat);
				cidade.setNoAccents(noAccents);
				cidade.setAlternativeNames(alternativeNames);
				cidade.setMicroRegion(microregion);
				cidade.setMesoRegion(mesoregion);

				cidades.add(cidade);

			}

			cidadeRepository.save(cidades);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return cidades;

	}

}
