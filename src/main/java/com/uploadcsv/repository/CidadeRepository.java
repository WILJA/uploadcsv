package com.uploadcsv.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.uploadcsv.domain.Cidade;

@RepositoryRestResource(exported = false)
public interface CidadeRepository extends JpaRepository<Cidade, Long> {	

	@Query("select c from Cidade c where UPPER(c.name) like %?1%")
	Page<Cidade> findByNameContaining(String descricao, Pageable pageable);
	
	@Query("select c from Cidade c where UPPER(c.name) like %?1% ")
	List<Cidade> findByNameContaining(String descricao);
	
	@Query("select new com.uploadcsv.dto.UfDto(count(c.uf), c.uf) from Cidade c group by c.uf")
	List<Cidade> findByCountUf();
}
