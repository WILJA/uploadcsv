package com.upload.util;

import java.util.Date;

import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

public class UploadCvsUtil {
	public static MultipartFile multipart(String url) {
		byte[] bytes = new byte[1024];
		String encodedImg = url.split(",")[1];
		String baseImage = url.split(",")[0];
		String contentType = baseImage.split(";")[0];
		bytes = Base64Utils.decodeFromString(encodedImg);
		return new Base64DecodedMultipartFile(bytes, "notification",
				"notification" + new Date().getTime() + "." + contentType.split(":")[1].split("/")[1],
				contentType.split(":")[1]);
	}
}
