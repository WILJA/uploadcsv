package com.sienge.transporte;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.uploadcsv.domain.Cidade;
import com.uploadcsv.repository.CidadeRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CidadeRepositoryTest {

	@Autowired
	TestEntityManager entityManager;
	
	@Autowired
	CidadeRepository repository;
	//1100015,RO,Alta Floresta D'Oeste,,-61.9998238963,-11.9355403048,Alta Floresta D'Oeste,,Cacoal,Leste Rondoniense
	@Test
	public void testSaveCidade() throws Exception {
		Cidade cidade = Cidade.builder()
				.ibgeId(1100015L).uf("RO").name("Alta Floresta D'Oeste").capital("").lon("-61.9998238963").lat("-11.9355403048")
				.noAccents("Alta Floresta D'Oeste").alternativeNames("").microRegion("Cacoal").mesoRegion("Leste Rondoniense").build();
		
		cidade = repository.save(cidade);
		
		assertNotNull(cidade);
		assertTrue(cidade.getId() != null);
	}
	
	@Test
	public void testDeleteCidade() throws Exception {
		Cidade cidade = entityManager.persist(Cidade.builder()
				.ibgeId(1100015L).uf("SC").name("CIDADE DELETE").capital("").lon("-11.9998238963").lat("-12.9355403048")
				.noAccents("CIDADE DELETE").alternativeNames("").microRegion("CIDADE DELETE").mesoRegion("CIDADE DELETE").build());
		
		repository.delete(cidade);		
		cidade = repository.findOne(cidade.getId());
		
		assertNull(cidade);
	}
	
	@Test
	public void testFindByName() throws Exception {
		entityManager.persistAndFlush(Cidade.builder()
				.ibgeId(1100015L).uf("SC").name("CIDADE PESQUISA").capital("").lon("-11.9998238963").lat("-12.9355403048")
				.noAccents("CIDADE PESQUISA").alternativeNames("").microRegion("CIDADE PESQUISA").mesoRegion("CIDADE PESQUISA").build());
		
		List<Cidade> cidades = repository.findByNameContaining("CIDADE PESQUISA");
		
		assertNotNull(cidades);
		assertFalse(cidades.isEmpty());
		assertTrue(cidades.get(0).getName().equals("CIDADE PESQUISA"));
	}
	
	
	

}
