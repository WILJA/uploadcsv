package com.sienge.transporte;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uploadcsv.controller.CidadeController;
import com.uploadcsv.domain.Cidade;

@RunWith(SpringRunner.class)
@WebMvcTest(CidadeController.class)
public class CidadeControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper jsonParser;

	@Test
	public void testGetAll() throws Exception {
		mockMvc.perform(get("/cidades")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.[0].id", equalTo(1)))
				.andExpect(jsonPath("$.[0].name", equalTo("Alta Floresta D'Oeste")));

	}

	@Test
	public void testGet() throws Exception {
		mockMvc.perform(get("/cidades/{id}", 1)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", equalTo(1)))
				.andExpect(jsonPath("$.name", equalTo("Alta Floresta D'Oeste")));
	}

	@Test
	public void testCreate() throws Exception {
		Cidade cidade = new Cidade(20L, 20L, "RS", "CARAZINHO", "", "00000", "11111", "CARAZINHO", "CARAZINHO",
				"CARAZINHO", "CARAZINHO");
		mockMvc.perform(post("/cidades").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(jsonParser.writeValueAsString(cidade))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.id", equalTo(20))).andExpect(jsonPath("$.name", equalTo("CARAZINHO")));

	}

}
